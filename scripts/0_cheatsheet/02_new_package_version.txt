// 1. CREATE A NEW PACKAGE
------------------------------------------------------------------------------------------
sfdx force:package:create --name Enxoo-RecipePackageName --description "Enxoo Recipe - PackageName" --packagetype Unlocked --path enxoo-RecipePackageName --nonamespace --targetdevhubusername DevHub

// 2. CREATE A NEW PACKAGE VERSION
------------------------------------------------------------------------------------------
sfdx force:package:version:create -p Enxoo-RecipePackageName -d enxoo-RecipePackageName --wait 25 --codecoverage

// 3. PROMOTE A PACKAGE VERSION
------------------------------------------------------------------------------------------
sfdx force:package:version:promote --package "Enxoo-RecipePackageName@0.1.0-4"