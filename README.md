# Enxoo - Recipe Template Repository

This repository contains an SFDX project template for development of "recipe" - an unlocked (2GP) package on top of other Enxoo Managed Packages.

### What is Recipe 

Enxoo Recipes are small "plugins" that are developed as unlocked packages - extensions to Enxoo Commerce / Enxoo for Communications designed to solve a particular problem. These extensions usually are a good example that can be used as a starting point to jump-start your own customization for a particular customer.

### Recipes Documentation and Installation Guide

https://enxooteam.atlassian.net/wiki/spaces/ECPQKB/pages/1842413786/Recipes
### Recipe Development Guidelines

https://enxooteam.atlassian.net/wiki/spaces/ECPQKB/pages/1941602602/Template+-+build+your+own+Recipe

## Development Process

### Naming convention

1. Commit: ECPQ-XXXX: message (what has been changed)
2. Branch: prefix/ECPQ-XXXX-message
    Available prefixes: bugfix, feature, hotfix
    One branch per task, bug etc.
3. Dev branch: dev
4. Pull request: created automatically from branch name
    Remember to check "Close branch after merge"

### Development cycle

1. To develop a feature, create a branch from dev (e.g. dev → feature/ECPQ-667)
2. When feature is tested by developer, create Pull Request to dev (Status: Development Done)
3. Resolve conflicts (merge dev into feature branch and resolve conflicts, then push code into your feature branch) *optional
4. After approval from Reviewer, close Pull Request (Status: Ready for QA)
5. Feature is tested by QA on dev branch *can be postponed due to release testing
6. After successful QA (Status: QA done), merge your feature branch into dev and change status to Done

### Release cycle

Currently there is no release cycle.
Packages listed in Package Index below can be released on demand.